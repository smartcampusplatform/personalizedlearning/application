SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `price` decimal(15,2) DEFAULT NULL,
  `course_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `published` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `title`, `slug`, `description`, `price`, `course_image`, `start_date`, `published`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ilene Collins', 'ilene-collins', 'Modi quis aliquam at qui rerum. Commodi alias dolorem explicabo nihil nobis. Rem incidunt repellendus harum quo voluptatem est praesentium.', '187.43', NULL, NULL, 1, '2019-05-14 19:35:06', '2019-05-14 21:18:50', '2019-05-14 21:18:50'),
(2, 'Juana Collins', 'juana-collins', 'Quidem fugiat illo quia modi. Dolores ut nulla qui est. Omnis blanditiis enim corrupti velit ea nihil dolores et. Et blanditiis ex nihil nihil. Dolores vero saepe accusamus.', '29.81', NULL, NULL, 1, '2019-05-14 19:35:06', '2019-05-14 21:18:50', '2019-05-14 21:18:50'),
(3, 'tes', 'tes', 'tes', NULL, NULL, '2019-05-14', 1, '2019-05-14 19:42:07', '2019-05-14 19:42:07', NULL),
(4, 'Course 1', 'course1', 'deskripsi course 1', NULL, NULL, '2019-05-14', 1, '2019-05-14 22:38:43', '2019-05-14 22:38:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `course_student`
--

CREATE TABLE `course_student` (
  `course_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `rating` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `course_user`
--

CREATE TABLE `course_user` (
  `course_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `course_user`
--

INSERT INTO `course_user` (`course_id`, `user_id`) VALUES
(1, 1),
(2, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `dokumen`
--

CREATE TABLE `dokumen` (
  `id` int(10) NOT NULL,
  `file` varchar(191) NOT NULL,
  `keterangan` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokumen`
--

INSERT INTO `dokumen` (`id`, `file`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, '1557806078_00-PengantarKuliahRekayasaPerangkatLunak.pdf', 'Materi 1 RPL', '2019-05-13 20:54:38', '2019-05-13 20:54:38'),
(2, '1557899117_SMART_SCHOOL_SYSTEM_ISSUES_AND_CHALLENGE.pdf', 'tugas smart school', '2019-05-14 22:45:17', '2019-05-14 22:45:17');

-- --------------------------------------------------------

--
-- Table structure for table `lessons`
--

CREATE TABLE `lessons` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lesson_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_text` text COLLATE utf8mb4_unicode_ci,
  `full_text` text COLLATE utf8mb4_unicode_ci,
  `position` int(10) UNSIGNED DEFAULT NULL,
  `free_lesson` tinyint(4) DEFAULT '0',
  `published` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lessons`
--

INSERT INTO `lessons` (`id`, `course_id`, `title`, `slug`, `lesson_image`, `short_text`, `full_text`, `position`, `free_lesson`, `published`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Non mollitia rerum consequatur blanditiis fuga.', 'non-mollitia-rerum-consequatur-blanditiis-fuga', NULL, 'Nesciunt sapiente autem qui culpa voluptas tempore itaque. Nesciunt in rerum dolorum. Consectetur qui aperiam veritatis dolor deleniti voluptatum. Optio est inventore iste et quia recusandae quas sapiente.', 'Dicta magni velit et veritatis quod animi. Rerum nihil autem ad facilis ut aut. Praesentium et reprehenderit dolor unde aut ut. Voluptatem quas velit quisquam magnam ipsam sapiente voluptas accusamus.\nConsequatur ea possimus ut non ut perspiciatis totam. Enim quasi natus ut saepe ea. Quis praesentium minima aut atque ut sed.\nNon quo ad voluptatem deserunt quaerat nesciunt. In et delectus aut ut voluptatem. Placeat nemo voluptatem voluptate est deleniti. Dolorem perspiciatis autem culpa.\nUt saepe magnam totam eum consequuntur ipsa voluptatum. Voluptas dicta voluptates ut et. Accusamus praesentium voluptatem quas omnis cupiditate debitis soluta.\nCommodi voluptatibus enim sed est sed temporibus nihil. Eaque dolore in aut quam ullam labore. Ea in exercitationem reprehenderit. Sit quas omnis necessitatibus ratione dolore soluta. Temporibus qui incidunt quibusdam quasi.', 8, 0, 1, '2019-05-14 19:35:06', '2019-05-14 21:18:24', '2019-05-14 21:18:24'),
(2, 1, 'Consequuntur voluptatem quia illum eos mollitia.', 'consequuntur-voluptatem-quia-illum-eos-mollitia', NULL, 'Repudiandae maxime dolorem officia rem dolores quis quis. Et aut quia molestias ducimus tempore. Ea impedit eveniet repellat aliquid ut explicabo nisi aut.', 'Quibusdam odio magnam dolorum iusto omnis. Sint nesciunt perspiciatis voluptas et. Aut eligendi quisquam amet non.\nSit officiis libero libero temporibus. Voluptates velit voluptatem dolorem. Deleniti quis eligendi excepturi qui quo dolores.\nDebitis aut esse veritatis molestiae praesentium velit. Odit unde tenetur accusantium autem sed dolorem rerum. Id ducimus rerum iure et. Cupiditate et in voluptatibus delectus.\nSaepe veniam dolor qui dolorum dolorem beatae voluptates. Omnis occaecati quia dicta adipisci autem. Est qui omnis aliquid dicta. Tempore temporibus quam est ipsum est.\nQuia molestiae tempora necessitatibus modi quos. Excepturi nobis debitis placeat et soluta. Distinctio officia iusto officiis modi minus.\nDicta alias cumque est voluptatem fugiat. Quibusdam quisquam dolores ullam corrupti ea ut quis. Optio velit sapiente ut praesentium dolores laboriosam.\nOfficiis culpa ea qui velit. Nostrum ut rerum rerum. Dolorem non et illo qui minima animi sint.', 10, 1, 0, '2019-05-14 19:35:06', '2019-05-14 21:18:24', '2019-05-14 21:18:24'),
(3, 1, 'Aspernatur ab ducimus et veniam.', 'aspernatur-ab-ducimus-et-veniam', NULL, 'Amet perspiciatis est porro aliquid esse voluptas qui. Delectus possimus et sit nulla consequatur rem. Magni omnis rerum fuga distinctio est recusandae.', 'Omnis consequatur id quia consectetur aut ut velit. Non ut et et temporibus suscipit. Molestiae totam laboriosam qui reiciendis dolor.\nQuo rerum in quod quibusdam. Praesentium quas ipsam deleniti accusantium molestiae autem sapiente temporibus. Magni voluptatum et quia voluptas quaerat ea. Voluptatem dolorem rerum aut soluta ea recusandae expedita.\nVelit omnis rem fugiat velit aliquid doloremque itaque aperiam. Quae numquam sint et nam repellat libero maxime ab. Quia saepe laboriosam est hic incidunt et adipisci. Aut sapiente eaque saepe similique.\nOmnis consequatur recusandae est voluptas sequi magnam dolor. Accusantium ea enim et et enim alias. Voluptate sed fugit sint sit quia id esse. Quo vitae suscipit neque aliquid dolores est nihil laboriosam.\nEnim saepe quasi ipsum et. Aut sed qui qui odio reprehenderit molestias. Eos ut accusamus sit iste omnis cum.\nEarum omnis sint consequuntur est nulla in cupiditate. Est aut a recusandae tempore qui. Corporis dolore maxime sed at.', 9, 0, 0, '2019-05-14 19:35:06', '2019-05-14 21:18:24', '2019-05-14 21:18:24'),
(4, 1, 'Natus ut aut distinctio veritatis.', 'natus-ut-aut-distinctio-veritatis', NULL, 'Aliquam sit aut nisi consequatur illo aliquam. Qui accusamus nulla at numquam quia. Rerum impedit excepturi aspernatur. Dolore excepturi vel nostrum amet tenetur est vero.', 'Explicabo dolorem excepturi aut in sapiente sapiente. Suscipit iusto maxime repudiandae veritatis. Quisquam esse quo qui quasi ab. Enim reprehenderit rerum quo voluptatibus.\nAlias quasi cumque qui amet voluptates est ad. Nam sint aspernatur maxime debitis iusto at voluptatem. Eum inventore cumque nemo provident est quod magni.\nNeque explicabo fugit aliquid incidunt ut et veniam. Expedita molestiae rem corporis consequuntur. Aut delectus harum qui fugiat amet esse soluta. Ad deleniti soluta et aspernatur mollitia.\nEos cum recusandae laborum nostrum architecto itaque eum. Eveniet voluptatibus voluptatem qui. Ipsa tempora hic iure aliquam. Dolore dolor expedita sed culpa corporis ipsa modi quis.\nSimilique enim consectetur quia est. Sit aliquid molestiae autem temporibus id explicabo. Qui sint animi similique optio. Non eum quos eveniet reprehenderit asperiores. Consequatur sit rerum in eveniet sed et.', 6, 1, 0, '2019-05-14 19:35:06', '2019-05-14 21:18:24', '2019-05-14 21:18:24'),
(5, 1, 'Enim non non tenetur iste non cumque ut.', 'enim-non-non-tenetur-iste-non-cumque-ut', NULL, 'Rerum vel ipsum labore in adipisci suscipit. Odio nulla reprehenderit repellat voluptas sed dicta et neque. Autem reiciendis eos excepturi qui doloremque numquam quod rerum. Architecto ipsa non quidem nihil. Accusantium dolor aspernatur rerum facere a velit quis.', 'Consequatur qui commodi deleniti animi. Totam id et qui voluptatibus aspernatur doloribus accusamus qui. Reprehenderit autem earum quia aliquid temporibus minus molestias.\nQui velit quaerat perferendis cumque. Ea repellendus quos vero quae aut aut et. Et vero quidem ea sed ipsa. Labore sunt assumenda ratione quos est asperiores aspernatur quia.\nSapiente exercitationem iste quos impedit est reiciendis quas. Ut omnis voluptas eos eum dolorum quidem adipisci. Sed voluptas consequatur nesciunt aut. Officiis sed consequatur veritatis accusantium quaerat similique quis. Sequi qui aut velit praesentium voluptatem recusandae.\nVoluptatibus necessitatibus minus quasi voluptas. Quis voluptatem et in aliquid. Et libero nemo voluptatum et aspernatur consequuntur. Et nesciunt soluta quod sed. Et sed dolorem eveniet dolorum eveniet ducimus nulla.', 6, 0, 1, '2019-05-14 19:35:06', '2019-05-14 21:18:24', '2019-05-14 21:18:24'),
(6, 1, 'Ut reprehenderit aliquam laborum consequatur et.', 'ut-reprehenderit-aliquam-laborum-consequatur-et', NULL, 'Repellat id magni ea repellendus necessitatibus. Autem repellat quisquam mollitia voluptatem. Fugit a quia rerum optio sit. Similique id ut autem repellat est.', 'Illo velit qui quisquam inventore tempore minima voluptatibus. Alias ut et harum labore et et et. Et voluptatem deleniti sequi nostrum ea aut provident. Accusamus facilis quia itaque est.\nOccaecati autem optio et cumque voluptatum. Quis sit dolor quaerat autem iste nemo dolores. Velit aliquam possimus laboriosam recusandae. Quia deleniti aperiam ut animi a natus. Veritatis eligendi excepturi sint atque distinctio mollitia.\nEveniet est illum quibusdam a voluptate at placeat. Reprehenderit quibusdam doloremque optio neque. Minima magni fugit hic rerum porro consequuntur cumque.\nExpedita culpa ut aut harum officia dolor neque. Sint soluta rerum dolor facilis omnis. Quasi alias quidem recusandae similique in enim quia.\nSequi eos natus maxime aut sit quam sunt. Distinctio ea repellat sapiente sit quia quo.\nAutem nulla enim sint et dolore enim dicta. Rerum quo quia facere in illo commodi. Ipsum pariatur suscipit cum perspiciatis corrupti ducimus recusandae. Et quisquam qui error.', 4, 1, 0, '2019-05-14 19:35:06', '2019-05-14 21:18:24', '2019-05-14 21:18:24'),
(7, 1, 'Ex eaque vel illum et ex qui consequatur.', 'ex-eaque-vel-illum-et-ex-qui-consequatur', NULL, 'Aliquid unde accusamus omnis dignissimos corporis ut. Hic quia beatae dolore neque blanditiis cupiditate. Rerum natus explicabo temporibus. Fuga deserunt fuga rerum non voluptatem asperiores.', 'Est praesentium maxime eius quos. Eum eum omnis nulla. Delectus dolorum architecto exercitationem saepe vitae quod enim.\nEst minima iste voluptatem et aut. Fugit ad facere hic rerum illum voluptas.\nSunt et iure consequatur pariatur. Qui sed ut nam id itaque et eum. Qui est et eum itaque sit architecto ab.\nNam in placeat dolor nulla possimus laudantium. Inventore quod repellendus omnis fuga magni deserunt voluptatum. Sit aliquid sed cupiditate commodi. Ut totam eligendi neque ad hic.\nNemo modi accusantium sunt rem maiores. Sed earum velit dolorum iure unde voluptas fugit. Hic vitae quis doloribus. Cumque qui non et ea.\nDistinctio aut esse ut iure. Amet mollitia optio vel nisi animi. Quaerat qui quia vitae sit eveniet.\nAnimi quis delectus similique dolor aut et. Iste pariatur ipsum quis dolores qui dolores. Incidunt ipsum in adipisci. Aut tempora reprehenderit maxime nam. Vel vitae nulla officia ipsum in libero.', 10, 1, 0, '2019-05-14 19:35:06', '2019-05-14 21:18:25', '2019-05-14 21:18:25'),
(8, 1, 'Consequatur fugit enim nulla a doloribus.', 'consequatur-fugit-enim-nulla-a-doloribus', NULL, 'Quo ad qui molestias ipsam. Dolore qui eveniet adipisci consequatur quibusdam pariatur qui impedit. Ullam hic nobis quis aut necessitatibus velit accusantium. Autem est assumenda molestias itaque suscipit.', 'Aut dignissimos sunt ut quos quo dolorum autem. Vero aperiam beatae iusto odit assumenda quia laudantium. Iusto ipsa autem eius eaque totam repudiandae.\nTempore ipsam itaque in nemo est corrupti consequatur voluptatem. Ratione reprehenderit ut laudantium eos natus fugit.\nEt voluptatem ad beatae sed voluptate deserunt. Quos voluptates aut omnis ut. Itaque odio suscipit et similique aperiam. Consequatur non eaque aut quia recusandae quia et.\nPerferendis repellendus minus veritatis dolores aut in ipsum earum. Recusandae cupiditate sed nihil beatae exercitationem fugit et. Vel totam inventore nemo. Aliquam mollitia minima eum nobis qui.\nIpsam libero doloremque aut vel expedita. Aut debitis explicabo alias perspiciatis. Aliquid quae dolorem voluptatem fuga et. Perspiciatis consequatur non quod perferendis numquam natus.\nMinus aut facere ducimus maiores mollitia sed deserunt soluta. Nobis aut asperiores sequi reprehenderit.\nLaboriosam voluptatem ex id ex vitae. Et labore quo occaecati.', 1, 1, 1, '2019-05-14 19:35:06', '2019-05-14 21:18:25', '2019-05-14 21:18:25'),
(9, 1, 'Repudiandae autem voluptas rem harum odit.', 'repudiandae-autem-voluptas-rem-harum-odit', NULL, 'Asperiores alias ullam dolorem sint. Aut doloribus voluptatem tempore facilis. Voluptate officiis ipsam officiis sit quaerat ducimus. Consequatur dolores enim qui enim sapiente.', 'Excepturi minus blanditiis ut perspiciatis enim dolorem. Voluptatem totam et occaecati id qui architecto voluptate. Est dolores vel quia accusamus qui neque. Tempora iusto et rerum.\nUt rerum dolorum molestiae tempora qui. Sint nihil consequatur velit sapiente eveniet. Placeat cupiditate numquam et velit atque aspernatur dolores. Sunt aut mollitia et accusamus earum saepe sit.\nBeatae velit soluta repudiandae error repudiandae facilis tempore. Quidem itaque in earum pariatur doloribus. Dolores nam vel facilis pariatur.\nBeatae voluptate voluptates non veniam asperiores quisquam tenetur eius. Porro voluptas voluptatibus blanditiis totam unde dolor. Facilis error cumque enim provident pariatur ex. Ut sed placeat consequatur eligendi a ut pariatur.\nEst eum laborum eos laudantium. Culpa fugit nisi deserunt ducimus. Aut voluptates consequatur repellat iusto quam doloremque incidunt.', 6, 0, 0, '2019-05-14 19:35:06', '2019-05-14 21:18:25', '2019-05-14 21:18:25'),
(10, 1, 'Autem voluptatem quia iste ea numquam.', 'autem-voluptatem-quia-iste-ea-numquam', NULL, 'Repudiandae sed ut facilis animi minima. Neque tenetur dolor consequatur necessitatibus consequatur sed aliquid. Non autem dolorem maiores pariatur.', 'Dolorem excepturi autem natus error alias blanditiis distinctio. Aut dicta numquam voluptatibus omnis voluptatem sed ad. Asperiores earum et sunt eius. Porro laudantium sint ea tempora ea voluptates iure voluptas.\nVeniam eveniet voluptatem nesciunt. Dicta omnis at qui laudantium recusandae consectetur. Ut sit delectus quo distinctio sit quia ut. Voluptas libero dolor iste aut.\nAliquid minus iusto dolorem voluptas beatae aut fuga. Magnam consequatur aut alias. Ipsam accusamus aut at veritatis et labore dolor.\nQuidem eum fugit in harum repudiandae quasi. Expedita quos et tempore qui mollitia tempora soluta consequatur. Tempore autem sint laborum asperiores vel error.\nQuo repudiandae ipsum omnis beatae necessitatibus officia blanditiis. Laudantium quisquam dolor dolor a. Est qui vitae impedit eaque corporis. Qui doloremque placeat voluptas voluptatum quae deserunt commodi cumque.\nVoluptatum placeat dolorem possimus nisi. Rerum voluptatem et ipsam rerum ullam quam.', 9, 0, 1, '2019-05-14 19:35:06', '2019-05-14 21:18:25', '2019-05-14 21:18:25'),
(11, 2, 'Ullam et eveniet distinctio veritatis qui.', 'ullam-et-eveniet-distinctio-veritatis-qui', NULL, 'Fugit assumenda et ipsum consequatur. Incidunt eos rerum velit tempore. Autem dolores modi doloribus vero. Eos aspernatur eos enim explicabo.', 'Non non amet blanditiis. Eum voluptatem amet fugiat molestias ut. Sit cumque ut sunt deleniti. Ea maiores tenetur totam recusandae.\nQuod qui sit vitae perferendis dicta. Molestias veritatis voluptas dignissimos ut sed ea. Expedita expedita quas molestias eum ad.\nVel ipsam sapiente rerum fugiat at. Molestias deserunt dolorum aut eveniet non neque et. Voluptatem eligendi corrupti eligendi iure.\nLaborum qui cupiditate quae delectus. Et in doloribus qui quis necessitatibus corrupti quia.\nEt dolor atque molestiae inventore porro sint labore. Cum molestiae et perferendis aperiam eius. Error odio sed qui sit qui illo. Deserunt aliquam ipsa omnis enim aut voluptas odio reiciendis.\nOdio eaque velit sit soluta nam veniam libero qui. Quasi cum iusto laudantium. Minus eum labore est accusantium quibusdam dignissimos. Ipsa unde reprehenderit veniam ut.', 6, 0, 0, '2019-05-14 19:35:08', '2019-05-14 21:18:25', '2019-05-14 21:18:25'),
(12, 2, 'Incidunt temporibus qui doloribus accusantium.', 'incidunt-temporibus-qui-doloribus-accusantium', NULL, 'Repudiandae vero dolore vel dolores ratione ipsum natus. Quod minima veniam voluptas sunt. Repellendus dolorum doloribus inventore consequuntur dolores nemo.', 'Laudantium neque vel labore labore. Qui nisi libero suscipit voluptatem repellendus. Sed ipsum et nam dolores assumenda aut.\nEos voluptates molestiae qui aspernatur officiis fugit veniam iure. Cum enim quibusdam dolor tempore facilis. Esse laboriosam magni soluta iure eius dolores molestias est.\nIusto esse eum aut voluptas est. Rerum voluptatem fugiat sed sit dolores aut. Voluptate nesciunt dolor et quos sequi iure non est.\nVelit delectus nemo sapiente. Iste ut magnam dolorem distinctio incidunt. Qui qui numquam inventore nemo distinctio.\nEius et quasi omnis minus in deleniti accusantium. Aut tempore ipsam odit. Libero et ut sapiente iusto omnis. Dolores enim est et iste suscipit.\nPlaceat minima magni deleniti qui non omnis in. Odio et et rerum ducimus sit nisi nihil. Illo eius qui rerum minus rerum in consequuntur.\nVero et eligendi corrupti possimus. Consequuntur dolores excepturi ut quis minus. Eum autem distinctio sit minima laboriosam. Quia nulla unde eos pariatur et.', 9, 0, 1, '2019-05-14 19:35:08', '2019-05-14 21:18:25', '2019-05-14 21:18:25'),
(13, 2, 'Harum laborum sed sunt aperiam.', 'harum-laborum-sed-sunt-aperiam', NULL, 'Rerum consequuntur molestias nostrum est quisquam quibusdam beatae. Sit et sunt non aut voluptatum velit. Ut dolorum consequatur dolor aut voluptates sed quo.', 'Ipsam nesciunt voluptas ipsam fugiat porro. Quidem aperiam debitis est et temporibus. Voluptatem doloremque tenetur illum qui. Cumque aliquid magni rerum fuga quis.\nAut in ducimus ab harum molestias. Ab aut eos est ut cupiditate. Qui provident recusandae quos facilis beatae ut quo. Veniam veniam sit fugit.\nQuisquam sit hic doloremque exercitationem cum qui quisquam. Ea vitae perferendis officia distinctio minima et praesentium labore. Consequatur explicabo totam et quia exercitationem.\nHarum quod qui aut eum odio. Esse voluptatem voluptas autem quos doloribus reprehenderit. Rerum omnis explicabo error maxime. Nulla nulla dolores sint qui rerum.\nQuasi deleniti molestiae amet perferendis. Dicta sit voluptatem sequi laborum at reiciendis inventore modi. Optio saepe vel sequi consequatur. Saepe dolore vel error non.\nSint repellendus magni qui dolor ut veniam omnis. Atque aut sint ipsum ut corrupti odio voluptate voluptatibus. Tempore quia autem est fuga ipsam omnis dicta.', 5, 1, 1, '2019-05-14 19:35:08', '2019-05-14 21:18:25', '2019-05-14 21:18:25'),
(14, 2, 'Et sed laborum eveniet voluptates esse.', 'et-sed-laborum-eveniet-voluptates-esse', NULL, 'Alias vitae aperiam aut consectetur esse vitae ipsum. Architecto perferendis eveniet quia nulla. Ut iure blanditiis est ea reprehenderit ipsum consequatur vel. Aliquid vel autem necessitatibus tenetur enim vitae consequatur.', 'Minus quis impedit voluptatem. Sed ut velit velit modi sequi iusto libero quas. Dolores totam dignissimos sapiente temporibus.\nSit quod ut quae totam. Dolore dolor cumque voluptatibus amet. Vel reprehenderit non aut eos autem quis.\nAut natus eos placeat rerum aut harum. Excepturi est consequatur rerum et. Dolore id laudantium eum.\nAmet deserunt incidunt nihil voluptas aut quia. Dolores qui enim cumque assumenda a aut excepturi totam. Qui nihil modi quod quia repellat voluptas. Qui ut quam deserunt culpa alias sequi maxime et.\nDolorum assumenda ullam ut consectetur labore. Minima sit ad eum quos id illum sit. Quos ratione laboriosam sunt culpa.\nVelit dolore beatae iste impedit libero dolorem. Dolorem incidunt cupiditate sed ratione. Non voluptatem dolor aut qui ad.\nIn odio molestiae et reprehenderit est assumenda quasi. Veniam omnis atque quam sed. Omnis quas non pariatur dolores enim.', 7, 0, 0, '2019-05-14 19:35:08', '2019-05-14 21:18:25', '2019-05-14 21:18:25'),
(15, 2, 'Sapiente non modi at dicta accusamus.', 'sapiente-non-modi-at-dicta-accusamus', NULL, 'Aut sed odio ut quas soluta et nisi. Esse harum debitis repellat delectus earum itaque. Voluptatem et sapiente voluptas nobis accusantium culpa deleniti. Qui ea distinctio quidem aspernatur qui rem voluptates.', 'Dolorem perspiciatis alias expedita voluptas alias. Quasi praesentium magni est molestiae porro doloremque est. Vitae cupiditate distinctio omnis dolorum recusandae suscipit. Ipsam dignissimos odit et voluptas excepturi corporis.\nEsse doloremque quibusdam in neque quis vero. Quibusdam ipsa voluptatem deleniti. Veniam voluptas necessitatibus ratione consequuntur vel accusantium illo. Reprehenderit aut aut quae minima.\nQuia ratione voluptatem sunt nam non dolore. Quibusdam iste et nam. Nulla rem ut et quia culpa minima. Sed rem cupiditate est. Id in consequatur voluptatem.\nEt ad quas eum distinctio dolorem exercitationem voluptas. A porro sed atque neque unde nisi quia. Exercitationem a non eius quia dolores eos nam. Voluptatem explicabo ipsum tempora et. At fuga reprehenderit placeat et quasi est.\nOfficia expedita sed et atque temporibus. Ad rerum error at veritatis quo. Aperiam culpa officiis itaque laborum tempora. Iusto adipisci suscipit commodi aut unde.', 6, 1, 0, '2019-05-14 19:35:08', '2019-05-14 21:18:25', '2019-05-14 21:18:25'),
(16, 2, 'Quidem qui vero aut eos placeat mollitia.', 'quidem-qui-vero-aut-eos-placeat-mollitia', NULL, 'Doloribus nihil maiores veniam vel. Maiores ducimus quo voluptates dolorum in soluta. Eaque velit similique ullam aliquid enim mollitia. Autem vero in impedit voluptates.', 'Fugiat iure quis laboriosam totam suscipit porro praesentium. Veniam quas enim iusto. Eius ipsam omnis omnis libero et placeat enim.\nOfficiis deleniti quisquam deleniti asperiores dolorem aut. Iste est quae iusto nisi itaque sed voluptatibus. Laudantium est et laboriosam doloribus sapiente rerum voluptatem distinctio. Aut facilis quo qui quisquam.\nEnim est nesciunt ducimus minima est. Aliquid ipsam minima in aliquam. Dolor recusandae voluptatem natus provident et quas est necessitatibus.\nIpsum similique cupiditate id pariatur tempora. Commodi voluptatem tempora earum vel. Voluptate nulla ipsam odio.\nDelectus excepturi explicabo rerum perferendis. Laboriosam corrupti minus vero molestiae ullam. Sit sint id quia rerum enim nam.\nAutem cum sit et doloremque. Ducimus incidunt amet quo quo. Blanditiis ut quia et et velit enim ut. Et labore rem nostrum soluta qui.\nQuos quam illum magnam. Consequatur placeat nihil aut enim.', 2, 0, 1, '2019-05-14 19:35:08', '2019-05-14 21:18:25', '2019-05-14 21:18:25'),
(17, 2, 'Aspernatur quas hic et perferendis fugit est.', 'aspernatur-quas-hic-et-perferendis-fugit-est', NULL, 'Dicta vero dolore quia. Voluptatum corrupti velit nihil cum. Illo ut provident reprehenderit quo. Tenetur aut dolores quisquam.', 'Tempore enim sit ipsam quis sint eligendi et. Consectetur qui ipsum natus corporis deserunt eligendi omnis. Tenetur at nemo illo recusandae.\nDolor maxime quibusdam corrupti eius. Quis suscipit et autem tempore ratione assumenda. Et provident consequatur quod dolores.\nQuibusdam accusamus est quo. Cum doloribus praesentium et et quia et perferendis optio. Et dolore atque laborum quibusdam sed et omnis libero. Deleniti veniam natus non dolorem et.\nVero enim ut voluptatem ea earum. Et ducimus perferendis minus a harum. Quisquam distinctio quis accusamus ullam consequatur.\nAspernatur in omnis eaque dolorem ex. Occaecati quasi architecto aliquid sint aut minima inventore. Cupiditate autem fuga ut voluptatem omnis qui. Molestiae ut omnis ratione. Eos sed eum rerum sint vero doloremque aliquam dicta.\nEos provident quo iure illum. Placeat illum nam iure odio provident adipisci dolorem. At hic deleniti tempora aut voluptate ea. Sint maiores repudiandae iusto incidunt.', 10, 1, 0, '2019-05-14 19:35:08', '2019-05-14 21:18:25', '2019-05-14 21:18:25'),
(18, 2, 'Voluptas dolorum dolorem nihil debitis.', 'voluptas-dolorum-dolorem-nihil-debitis', NULL, 'Doloremque expedita pariatur ut a aut. Qui nulla fugiat quia distinctio. Voluptas doloremque nostrum quam suscipit. Magni consequuntur ut reiciendis sit maxime possimus dignissimos.', 'Dolores qui et assumenda vel repellat quidem ipsa. Similique in in molestiae quae id ex. Laudantium error quibusdam neque dolor odit dolores ipsa.\nMolestias temporibus maiores commodi in et expedita assumenda tempora. Iste voluptas quae tempore hic aliquid nesciunt et autem. Impedit eos quam dolores incidunt qui ipsam et. Consequatur sed consequuntur voluptas quos.\nQuo aut vero facilis enim at molestias quam. Est possimus quidem maxime. Incidunt quod ut est culpa omnis ipsa ipsam.\nInventore voluptatem ut soluta rerum consequuntur quibusdam. Qui ipsum dignissimos quis sed ipsum eum aperiam. Iste aspernatur eum hic. Sed perferendis cum ut illo fugit.\nSimilique harum soluta est ipsam qui facilis. Voluptatum aut itaque magnam vero molestias non quos. Quia voluptatem ab esse rerum omnis ea libero et.\nAperiam unde omnis voluptatum ullam reprehenderit. Adipisci enim praesentium modi voluptas et. Atque repellendus nisi inventore alias debitis qui.', 5, 1, 1, '2019-05-14 19:35:08', '2019-05-14 21:18:25', '2019-05-14 21:18:25'),
(19, 2, 'Sint ducimus ut laboriosam.', 'sint-ducimus-ut-laboriosam', NULL, 'Iure laudantium aut sit. Nihil incidunt facere earum sit atque iure. Sunt voluptate et et laborum tenetur animi. Alias eum commodi consequuntur enim eos modi temporibus.', 'Rerum dolorum porro maxime hic. Tenetur minima explicabo similique. Impedit voluptatibus voluptate quam odit incidunt maxime.\nMagni nihil explicabo minus aut et asperiores quisquam. Consequatur reiciendis sed aut facilis sit maiores tempora. Molestiae nulla odio quo rerum consequatur harum. Enim perferendis ea eos deserunt natus qui.\nMaxime laudantium cum sed eveniet possimus qui. Magni aspernatur dolor sed libero aperiam consequatur. Reprehenderit similique ducimus dignissimos magni. Ipsa velit fuga eum corporis ut eos ducimus quia.\nEst sit iusto sed laudantium ea porro. Voluptates dolores enim omnis eveniet earum quia. Quos non illum molestiae veritatis. Tenetur aperiam at possimus ab voluptatum sint. Recusandae doloremque aut enim non cumque provident velit.\nVoluptatem aut similique praesentium. Pariatur cumque fuga ducimus. Asperiores praesentium id cum quis. Unde minima in soluta ad ipsa voluptas.', 5, 1, 0, '2019-05-14 19:35:08', '2019-05-14 21:18:25', '2019-05-14 21:18:25'),
(20, 2, 'Nostrum magni provident dolor id ut architecto.', 'nostrum-magni-provident-dolor-id-ut-architecto', NULL, 'Consectetur consectetur non reiciendis nobis eius beatae. Dignissimos dolor atque consequuntur repudiandae et nemo. Expedita vel esse et id dolorem possimus. Necessitatibus eum omnis omnis sit provident et ut.', 'Sapiente tenetur eveniet aut culpa accusamus doloribus odit. Eos voluptas impedit fugit incidunt cumque. Saepe aliquam voluptate rerum veniam. Est modi et sapiente corrupti.\nDoloribus soluta in maiores iure ratione. Est nisi qui possimus sed. Est dolorem et quaerat sunt voluptatum rem nam.\nAccusamus magnam expedita ut ipsum quae sit minima. Vel tempore aut sint. Culpa dolor nostrum illo aut.\nVel iusto impedit officia dolor eos sunt et iste. Et quaerat enim magnam minima. Optio exercitationem sed et error id dolores.\nEt dolor quasi voluptas deleniti laudantium. Impedit quod laborum distinctio debitis voluptas. Odio inventore inventore velit.\nNeque provident unde qui. Officiis animi aliquam officiis est error cum dolores. Blanditiis neque maxime eligendi.\nAtque non est officiis quia. Veritatis nemo quis sapiente occaecati ut perferendis. Placeat minus iste et vitae voluptatem suscipit et.', 3, 1, 1, '2019-05-14 19:35:08', '2019-05-14 21:18:25', '2019-05-14 21:18:25'),
(21, 3, 'tes', 'tes', NULL, 'tes', '<p>tes</p>', 1, 1, 1, '2019-05-14 19:42:32', '2019-05-14 19:42:32', NULL),
(22, 4, 'Materi pekan 1', 'materipekan1', NULL, 'materi pekan 1', '<p>tes</p>', 1, 1, 1, '2019-05-14 22:39:59', '2019-05-14 22:39:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lesson_student`
--

CREATE TABLE `lesson_student` (
  `lesson_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lesson_student`
--

INSERT INTO `lesson_student` (`lesson_id`, `user_id`, `created_at`, `updated_at`) VALUES
(21, 2, '2019-05-14 19:43:35', '2019-05-14 19:43:35'),
(21, 3, '2019-05-14 19:45:05', '2019-05-14 19:45:05'),
(21, 1, '2019-05-14 21:19:14', '2019-05-14 21:19:14'),
(22, 1, '2019-05-14 22:40:06', '2019-05-14 22:40:06');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_properties` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_id`, `model_type`, `collection_name`, `name`, `file_name`, `disk`, `size`, `manipulations`, `custom_properties`, `order_column`, `created_at`, `updated_at`) VALUES
(1, 21, 'App\\Lesson', 'downloadable_files', 'Notulensi Kelompok 1', 'Notulensi Kelompok 1.pdf', 'media', 33361, '[]', '[]', 1, '2019-05-14 19:42:27', '2019-05-14 19:42:32');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2017_07_19_082005_create_1500441605_permissions_table', 1),
(3, '2017_07_19_082006_create_1500441606_roles_table', 1),
(4, '2017_07_19_082009_create_1500441609_users_table', 1),
(5, '2017_07_19_082347_create_1500441827_courses_table', 1),
(6, '2017_07_19_082723_create_1500442043_lessons_table', 1),
(7, '2017_07_19_082724_create_media_table', 1),
(8, '2017_07_19_082929_create_1500442169_questions_table', 1),
(9, '2017_07_19_083047_create_1500442247_questions_options_table', 1),
(10, '2017_07_19_083236_create_1500442356_tests_table', 1),
(11, '2017_07_19_120427_create_596eec08307cd_permission_role_table', 1),
(12, '2017_07_19_120430_create_596eec0af366b_role_user_table', 1),
(13, '2017_07_19_120808_create_596eece522a6e_course_user_table', 1),
(14, '2017_07_19_121657_create_596eeef709839_question_test_table', 1),
(15, '2017_08_14_085956_create_course_students_table', 1),
(16, '2017_08_17_051131_create_tests_results_table', 1),
(17, '2017_08_17_051254_create_tests_results_answers_table', 1),
(18, '2017_08_18_054622_create_lesson_student_table', 1),
(19, '2017_08_18_060324_add_rating_to_course_student_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'user_management_access', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(2, 'user_management_create', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(3, 'user_management_edit', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(4, 'user_management_view', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(5, 'user_management_delete', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(6, 'permission_access', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(7, 'permission_create', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(8, 'permission_edit', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(9, 'permission_view', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(10, 'permission_delete', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(11, 'role_access', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(12, 'role_create', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(13, 'role_edit', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(14, 'role_view', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(15, 'role_delete', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(16, 'user_access', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(17, 'user_create', '2019-05-14 19:35:00', '2019-05-14 19:35:00'),
(18, 'user_edit', '2019-05-14 19:35:01', '2019-05-14 19:35:01'),
(19, 'user_view', '2019-05-14 19:35:01', '2019-05-14 19:35:01'),
(20, 'user_delete', '2019-05-14 19:35:01', '2019-05-14 19:35:01'),
(21, 'course_access', '2019-05-14 19:35:01', '2019-05-14 19:35:01'),
(22, 'course_create', '2019-05-14 19:35:01', '2019-05-14 19:35:01'),
(23, 'course_edit', '2019-05-14 19:35:01', '2019-05-14 19:35:01'),
(24, 'course_view', '2019-05-14 19:35:01', '2019-05-14 19:35:01'),
(25, 'course_delete', '2019-05-14 19:35:01', '2019-05-14 19:35:01'),
(26, 'lesson_access', '2019-05-14 19:35:01', '2019-05-14 19:35:01'),
(27, 'lesson_create', '2019-05-14 19:35:01', '2019-05-14 19:35:01'),
(28, 'lesson_edit', '2019-05-14 19:35:01', '2019-05-14 19:35:01'),
(29, 'lesson_view', '2019-05-14 19:35:01', '2019-05-14 19:35:01'),
(30, 'lesson_delete', '2019-05-14 19:35:01', '2019-05-14 19:35:01'),
(31, 'question_access', '2019-05-14 19:35:01', '2019-05-14 19:35:01'),
(32, 'question_create', '2019-05-14 19:35:01', '2019-05-14 19:35:01'),
(33, 'question_edit', '2019-05-14 19:35:02', '2019-05-14 19:35:02'),
(34, 'question_view', '2019-05-14 19:35:02', '2019-05-14 19:35:02'),
(35, 'question_delete', '2019-05-14 19:35:02', '2019-05-14 19:35:02'),
(36, 'questions_option_access', '2019-05-14 19:35:02', '2019-05-14 19:35:02'),
(37, 'questions_option_create', '2019-05-14 19:35:02', '2019-05-14 19:35:02'),
(38, 'questions_option_edit', '2019-05-14 19:35:02', '2019-05-14 19:35:02'),
(39, 'questions_option_view', '2019-05-14 19:35:02', '2019-05-14 19:35:02'),
(40, 'questions_option_delete', '2019-05-14 19:35:02', '2019-05-14 19:35:02'),
(41, 'test_access', '2019-05-14 19:35:02', '2019-05-14 19:35:02'),
(42, 'test_create', '2019-05-14 19:35:02', '2019-05-14 19:35:02'),
(43, 'test_edit', '2019-05-14 19:35:02', '2019-05-14 19:35:02'),
(44, 'test_view', '2019-05-14 19:35:02', '2019-05-14 19:35:02'),
(45, 'test_delete', '2019-05-14 19:35:02', '2019-05-14 19:35:02');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED DEFAULT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(1, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(26, 2),
(27, 2),
(28, 2),
(29, 2),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(43, 2),
(44, 2),
(45, 2),
(1, 3),
(21, 3),
(24, 3),
(26, 3),
(29, 3),
(31, 3),
(34, 3),
(36, 3),
(37, 3),
(38, 3),
(39, 3),
(40, 3),
(41, 3),
(44, 3),
(25, 2),
(30, 2),
(35, 2);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `question_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `question_image`, `score`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ea ut ut aliquid temporibus amet vero.?', NULL, 1, '2019-05-14 19:35:09', '2019-05-14 19:44:46', '2019-05-14 19:44:46'),
(2, 'Alias dolorem neque quia qui.?', NULL, 1, '2019-05-14 19:35:09', '2019-05-14 19:44:46', '2019-05-14 19:44:46'),
(3, 'Error qui vel quis nisi fugiat.?', NULL, 1, '2019-05-14 19:35:10', '2019-05-14 19:44:46', '2019-05-14 19:44:46'),
(4, 'Quae rerum libero et.?', NULL, 1, '2019-05-14 19:35:10', '2019-05-14 19:44:46', '2019-05-14 19:44:46'),
(5, 'Ipsam inventore nostrum voluptates magni sit.?', NULL, 1, '2019-05-14 19:35:10', '2019-05-14 19:44:46', '2019-05-14 19:44:46'),
(6, 'tes', NULL, 10, '2019-05-14 19:43:26', '2019-05-14 19:43:26', NULL),
(7, 'apa itu service', NULL, 1, '2019-05-14 22:43:46', '2019-05-14 22:44:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `questions_options`
--

CREATE TABLE `questions_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED DEFAULT NULL,
  `option_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `correct` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions_options`
--

INSERT INTO `questions_options` (`id`, `question_id`, `option_text`, `correct`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Dolores vero aut et enim.?', 0, '2019-05-14 19:35:10', '2019-05-14 19:35:10', NULL),
(2, 1, 'Quidem eum tempora laborum qui libero.?', 1, '2019-05-14 19:35:10', '2019-05-14 19:35:10', NULL),
(3, 1, 'Odit ea voluptas occaecati quisquam fuga et et.?', 1, '2019-05-14 19:35:10', '2019-05-14 19:35:10', NULL),
(4, 1, 'Explicabo nam atque autem aut.?', 1, '2019-05-14 19:35:10', '2019-05-14 19:35:10', NULL),
(5, 2, 'Voluptatibus voluptatem ut ut deleniti.?', 1, '2019-05-14 19:35:10', '2019-05-14 19:35:10', NULL),
(6, 2, 'Enim voluptas laudantium distinctio.?', 0, '2019-05-14 19:35:10', '2019-05-14 19:35:10', NULL),
(7, 2, 'Recusandae aut unde deserunt veniam omnis.?', 1, '2019-05-14 19:35:10', '2019-05-14 19:35:10', NULL),
(8, 2, 'Fugiat doloremque ut molestias hic autem.?', 1, '2019-05-14 19:35:10', '2019-05-14 19:35:10', NULL),
(9, 3, 'Doloremque earum nesciunt iure praesentium.?', 1, '2019-05-14 19:35:11', '2019-05-14 19:35:11', NULL),
(10, 3, 'Aperiam quia sed distinctio aliquid assumenda.?', 1, '2019-05-14 19:35:11', '2019-05-14 19:35:11', NULL),
(11, 3, 'Voluptatibus omnis consequatur qui dicta.?', 1, '2019-05-14 19:35:11', '2019-05-14 19:35:11', NULL),
(12, 3, 'Tenetur temporibus eos consequatur rem.?', 1, '2019-05-14 19:35:11', '2019-05-14 19:35:11', NULL),
(13, 4, 'Vel doloremque ratione a quod.?', 1, '2019-05-14 19:35:11', '2019-05-14 19:35:11', NULL),
(14, 4, 'Enim non sit cumque fuga atque eos.?', 0, '2019-05-14 19:35:11', '2019-05-14 19:35:11', NULL),
(15, 4, 'Non ut id commodi nostrum odio nulla.?', 0, '2019-05-14 19:35:11', '2019-05-14 19:35:11', NULL),
(16, 4, 'Nihil ut facere est nobis quaerat ducimus error.?', 1, '2019-05-14 19:35:11', '2019-05-14 19:35:11', NULL),
(17, 5, 'Eos commodi tenetur tempore sapiente.?', 1, '2019-05-14 19:35:11', '2019-05-14 19:35:12', NULL),
(18, 5, 'In laudantium architecto aut natus quis sit.?', 0, '2019-05-14 19:35:11', '2019-05-14 19:35:12', NULL),
(19, 5, 'Voluptates eum recusandae totam aut sequi.?', 0, '2019-05-14 19:35:11', '2019-05-14 19:35:12', NULL),
(20, 5, 'Eum quia quod iste ut consequatur et ex sed.?', 1, '2019-05-14 19:35:11', '2019-05-14 19:35:12', NULL),
(21, 6, 'tes', 1, '2019-05-14 19:43:26', '2019-05-14 19:43:26', NULL),
(22, 6, 'tes', 0, '2019-05-14 19:43:26', '2019-05-14 19:43:26', NULL),
(23, 7, 'ini pilihan benar', 1, '2019-05-14 22:43:46', '2019-05-14 22:43:46', NULL),
(24, 7, 'ini pilihan salah', 0, '2019-05-14 22:43:46', '2019-05-14 22:43:46', NULL),
(25, 7, 'pilihan salah 2', 0, '2019-05-14 22:43:46', '2019-05-14 22:43:46', NULL),
(26, 7, 'pilihan salah 3', 0, '2019-05-14 22:43:46', '2019-05-14 22:43:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `question_test`
--

CREATE TABLE `question_test` (
  `question_id` int(10) UNSIGNED DEFAULT NULL,
  `test_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `question_test`
--

INSERT INTO `question_test` (`question_id`, `test_id`) VALUES
(1, 4),
(2, 4),
(3, 1),
(4, 2),
(5, 3),
(6, 21),
(7, 22);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Admin (Teacher)', '2019-05-14 19:35:02', '2019-05-14 21:35:28'),
(2, 'Students', '2019-05-14 19:35:02', '2019-05-14 21:35:36'),
(3, 'Guest', '2019-05-14 19:35:02', '2019-05-14 21:32:48');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED DEFAULT NULL,
  `lesson_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `published` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `course_id`, `lesson_id`, `title`, `description`, `published`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 1, 'Inventore aliquid et voluptatem expedita fugiat.', NULL, 1, '2019-05-14 19:35:06', '2019-05-14 19:42:46', '2019-05-14 19:42:46'),
(2, NULL, 2, 'Et unde inventore illum accusamus aspernatur.', NULL, 1, '2019-05-14 19:35:06', '2019-05-14 19:42:46', '2019-05-14 19:42:46'),
(3, NULL, 3, 'Occaecati cumque non quisquam.', NULL, 1, '2019-05-14 19:35:07', '2019-05-14 19:42:46', '2019-05-14 19:42:46'),
(4, NULL, 4, 'Eligendi aut fuga fugiat aut soluta.', NULL, 1, '2019-05-14 19:35:07', '2019-05-14 19:42:46', '2019-05-14 19:42:46'),
(5, NULL, 5, 'Vel eum eos inventore dicta harum rem.', NULL, 1, '2019-05-14 19:35:07', '2019-05-14 19:42:46', '2019-05-14 19:42:46'),
(6, NULL, 6, 'Vero dolores molestiae molestiae quaerat et.', NULL, 1, '2019-05-14 19:35:07', '2019-05-14 19:42:46', '2019-05-14 19:42:46'),
(7, NULL, 7, 'Laudantium sed magni est.', NULL, 1, '2019-05-14 19:35:07', '2019-05-14 19:42:46', '2019-05-14 19:42:46'),
(8, NULL, 8, 'Earum et omnis nihil odit expedita laborum.', NULL, 1, '2019-05-14 19:35:07', '2019-05-14 19:42:46', '2019-05-14 19:42:46'),
(9, NULL, 9, 'Quaerat eum dolores magnam vero qui voluptatum.', NULL, 1, '2019-05-14 19:35:07', '2019-05-14 19:42:46', '2019-05-14 19:42:46'),
(10, NULL, 10, 'Ut aut veritatis reprehenderit inventore commodi.', NULL, 1, '2019-05-14 19:35:07', '2019-05-14 19:42:46', '2019-05-14 19:42:46'),
(11, NULL, 11, 'Aut velit non voluptate dolores.', NULL, 1, '2019-05-14 19:35:08', '2019-05-14 19:42:46', '2019-05-14 19:42:46'),
(12, NULL, 12, 'Delectus maiores est quam minus nemo.', NULL, 1, '2019-05-14 19:35:08', '2019-05-14 19:42:46', '2019-05-14 19:42:46'),
(13, NULL, 13, 'Laudantium ut quia cumque consectetur.', NULL, 1, '2019-05-14 19:35:08', '2019-05-14 19:42:46', '2019-05-14 19:42:46'),
(14, NULL, 14, 'Est at magnam illo qui earum.', NULL, 1, '2019-05-14 19:35:08', '2019-05-14 19:42:46', '2019-05-14 19:42:46'),
(15, NULL, 15, 'Esse earum eveniet esse.', NULL, 1, '2019-05-14 19:35:09', '2019-05-14 19:42:47', '2019-05-14 19:42:47'),
(16, NULL, 16, 'Eos optio qui est est quaerat voluptas ullam.', NULL, 1, '2019-05-14 19:35:09', '2019-05-14 19:42:47', '2019-05-14 19:42:47'),
(17, NULL, 17, 'Soluta quo earum nemo cupiditate sed cumque.', NULL, 1, '2019-05-14 19:35:09', '2019-05-14 19:42:47', '2019-05-14 19:42:47'),
(18, NULL, 18, 'Modi rem magni culpa voluptas molestiae.', NULL, 1, '2019-05-14 19:35:09', '2019-05-14 19:42:47', '2019-05-14 19:42:47'),
(19, NULL, 19, 'Qui doloribus maxime similique.', NULL, 1, '2019-05-14 19:35:09', '2019-05-14 19:42:47', '2019-05-14 19:42:47'),
(20, NULL, 20, 'Neque perferendis et sit velit consequatur sed.', NULL, 1, '2019-05-14 19:35:09', '2019-05-14 19:42:47', '2019-05-14 19:42:47'),
(21, 3, 21, 'tes', 'tes', 1, '2019-05-14 19:42:56', '2019-05-14 19:42:56', NULL),
(22, 4, 22, 'Quiz 1', 'kuis pekan 1', 1, '2019-05-14 22:42:59', '2019-05-14 22:42:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tests_results`
--

CREATE TABLE `tests_results` (
  `id` int(10) UNSIGNED NOT NULL,
  `test_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `test_result` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tests_results`
--

INSERT INTO `tests_results` (`id`, `test_id`, `user_id`, `test_result`, `created_at`, `updated_at`) VALUES
(3, 21, 2, 10, '2019-05-14 22:09:34', '2019-05-14 22:09:34'),
(4, 22, 1, 10, '2019-05-14 22:44:10', '2019-05-14 22:44:10');

-- --------------------------------------------------------

--
-- Table structure for table `tests_results_answers`
--

CREATE TABLE `tests_results_answers` (
  `id` int(10) UNSIGNED NOT NULL,
  `tests_result_id` int(10) UNSIGNED DEFAULT NULL,
  `question_id` int(10) UNSIGNED DEFAULT NULL,
  `option_id` int(10) UNSIGNED DEFAULT NULL,
  `correct` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tests_results_answers`
--

INSERT INTO `tests_results_answers` (`id`, `tests_result_id`, `question_id`, `option_id`, `correct`, `created_at`, `updated_at`) VALUES
(3, 3, 6, 21, 1, '2019-05-14 22:09:34', '2019-05-14 22:09:34'),
(4, 4, 7, 23, 1, '2019-05-14 22:44:10', '2019-05-14 22:44:10');

-- --------------------------------------------------------

--
-- Table structure for table `timeline`
--

CREATE TABLE `timeline` (
  `id` int(10) NOT NULL,
  `pekan_ke` int(2) NOT NULL,
  `tanggal` date NOT NULL,
  `waktu` time NOT NULL,
  `judul` varchar(191) NOT NULL,
  `deskripsi` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timeline`
--

INSERT INTO `timeline` (`id`, `pekan_ke`, `tanggal`, `waktu`, `judul`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-05-12', '08:00:00', 'mengerjakan quiz 1 RekSTI', 'Quiz 1 RekSTI merupakan quiz pertama yang dilaksanakan di pekan pertama oleh dosen.', NULL, NULL),
(2, 1, '2019-05-13', '09:00:00', 'mengumpulkan tugas 1 DasKen', 'Tugas 1 DasKen merupakan tugas memprediksi nilai y test.', NULL, NULL),
(3, 2, '2019-05-14', '13:00:00', 'mendownload materi terbaru KI', 'Materi ini merupakan lanjutan dari kriptografi pekan lalu.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', '$2y$10$l4MghrLnKXTRUDlR07XQeesKHRIaAe7WzDf90g751BEf70AwnJ5m.', 'M4BI2BEtHQj7kToru4fBZL87DuaK6HZCse8EXu0Hcd2zxddmWqGKH1vZnZTC', '2019-05-14 19:35:02', '2019-05-14 19:35:02'),
(2, 'murid2', 'murid2@murid.com', '$2y$10$ANM1mX3GVoQfpVnU5T./SeaA9RNzrLRMgvHJdvz/12PbrxUkBEqEu', 'aWLTcb5tKaWWvSine2Zcv1DmqkythGFYBy97a8t9znw3edxSOjBxGjHNcQ4B', '2019-05-14 19:41:15', '2019-05-14 22:05:09'),
(3, 'murid', 'murid@murid.com', '$2y$10$p593d0xHbvSLD1gmvfsJEOgvk/QIFUYc6.hjwj5yg/anx4yseOW/a', 'ozAsrw1HhyGIYH1608AauwzEgNY7XZpTDdTrfYQbXiBP0l2ja9XjZ6YOvosd', '2019-05-14 19:41:26', '2019-05-14 19:41:26'),
(4, 'Dosen 1', 'dosen1@dosen.com', '$2y$10$NvqPDlmkvltNgaAWWYtkrO3dxlQYkWw7oDzVGrkTRQKSEzadRrCem', NULL, '2019-05-14 22:37:29', '2019-05-14 22:37:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `courses_deleted_at_index` (`deleted_at`);

--
-- Indexes for table `course_student`
--
ALTER TABLE `course_student`
  ADD KEY `course_student_course_id_foreign` (`course_id`),
  ADD KEY `course_student_user_id_foreign` (`user_id`);

--
-- Indexes for table `course_user`
--
ALTER TABLE `course_user`
  ADD KEY `fk_p_54418_54417_user_cou_596eece522b73` (`course_id`),
  ADD KEY `fk_p_54417_54418_course_u_596eece522bee` (`user_id`);

--
-- Indexes for table `dokumen`
--
ALTER TABLE `dokumen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lessons`
--
ALTER TABLE `lessons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `54419_596eedbb6686e` (`course_id`),
  ADD KEY `lessons_deleted_at_index` (`deleted_at`);

--
-- Indexes for table `lesson_student`
--
ALTER TABLE `lesson_student`
  ADD KEY `lesson_student_lesson_id_foreign` (`lesson_id`),
  ADD KEY `lesson_student_user_id_foreign` (`user_id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD KEY `fk_p_54415_54416_role_per_596eec08308d0` (`permission_id`),
  ADD KEY `fk_p_54416_54415_permissi_596eec0830986` (`role_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_deleted_at_index` (`deleted_at`);

--
-- Indexes for table `questions_options`
--
ALTER TABLE `questions_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `54421_596eee8745a1e` (`question_id`),
  ADD KEY `questions_options_deleted_at_index` (`deleted_at`);

--
-- Indexes for table `question_test`
--
ALTER TABLE `question_test`
  ADD KEY `fk_p_54420_54422_test_que_596eeef70992f` (`question_id`),
  ADD KEY `fk_p_54422_54420_question_596eeef7099af` (`test_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `fk_p_54416_54417_user_rol_596eec0af3746` (`role_id`),
  ADD KEY `fk_p_54417_54416_role_use_596eec0af37c1` (`user_id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `54422_596eeef514d00` (`course_id`),
  ADD KEY `54422_596eeef53411a` (`lesson_id`),
  ADD KEY `tests_deleted_at_index` (`deleted_at`);

--
-- Indexes for table `tests_results`
--
ALTER TABLE `tests_results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tests_results_test_id_foreign` (`test_id`),
  ADD KEY `tests_results_user_id_foreign` (`user_id`);

--
-- Indexes for table `tests_results_answers`
--
ALTER TABLE `tests_results_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tests_results_answers_tests_result_id_foreign` (`tests_result_id`),
  ADD KEY `tests_results_answers_question_id_foreign` (`question_id`),
  ADD KEY `tests_results_answers_option_id_foreign` (`option_id`);

--
-- Indexes for table `timeline`
--
ALTER TABLE `timeline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `dokumen`
--
ALTER TABLE `dokumen`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lessons`
--
ALTER TABLE `lessons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `questions_options`
--
ALTER TABLE `questions_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tests_results`
--
ALTER TABLE `tests_results`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tests_results_answers`
--
ALTER TABLE `tests_results_answers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `timeline`
--
ALTER TABLE `timeline`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `course_student`
--
ALTER TABLE `course_student`
  ADD CONSTRAINT `course_student_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `course_student_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `course_user`
--
ALTER TABLE `course_user`
  ADD CONSTRAINT `fk_p_54417_54418_course_u_596eece522bee` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_p_54418_54417_user_cou_596eece522b73` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `lessons`
--
ALTER TABLE `lessons`
  ADD CONSTRAINT `54419_596eedbb6686e` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `lesson_student`
--
ALTER TABLE `lesson_student`
  ADD CONSTRAINT `lesson_student_lesson_id_foreign` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `lesson_student_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `fk_p_54415_54416_role_per_596eec08308d0` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_p_54416_54415_permissi_596eec0830986` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `questions_options`
--
ALTER TABLE `questions_options`
  ADD CONSTRAINT `54421_596eee8745a1e` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `question_test`
--
ALTER TABLE `question_test`
  ADD CONSTRAINT `fk_p_54420_54422_test_que_596eeef70992f` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_p_54422_54420_question_596eeef7099af` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `fk_p_54416_54417_user_rol_596eec0af3746` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_p_54417_54416_role_use_596eec0af37c1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tests`
--
ALTER TABLE `tests`
  ADD CONSTRAINT `54422_596eeef514d00` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `54422_596eeef53411a` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tests_results`
--
ALTER TABLE `tests_results`
  ADD CONSTRAINT `tests_results_test_id_foreign` FOREIGN KEY (`test_id`) REFERENCES `tests` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tests_results_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tests_results_answers`
--
ALTER TABLE `tests_results_answers`
  ADD CONSTRAINT `tests_results_answers_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `questions_options` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tests_results_answers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tests_results_answers_tests_result_id_foreign` FOREIGN KEY (`tests_result_id`) REFERENCES `tests_results` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
