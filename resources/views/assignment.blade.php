@extends('layouts.app')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Assignment
        <small>Task upload</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Assignment</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="container">

            <h2 class="text-center my-5">Your Assignments</h2>

            <div class="col-lg-8 mx-auto my-5">

                @if(count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                    {{ $error }} <br />
                    @endforeach
                </div>
                @endif

                <form action="/assignment/proses" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <b>Document File</b><br />
                        <input type="file" name="file">
                    </div>

                    <div class="form-group">
                        <b>Description</b>
                        <textarea class="form-control" name="keterangan"></textarea>
                    </div>

                    <input type="submit" value="Upload" class="btn btn-primary">
                </form>

                <h4 class="my-5">Submitted</h4>

                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th width="1%">Type</th>
                            <th>Description</th>
                            <th width="1%">Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dokumen as $d)
                        <tr>
                            <td><img width="25px" src="/tugas/icon-354352_640.png"></td>
                            <td>{{$d->keterangan}}</td>
                            <td><a class="btn btn-danger" href="/assignment/hapus/{{ $d->id }}">Delete</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

@endsection
