<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
 
use App\Dokumen;
use File;
 
class UploadController extends Controller
{
	public function upload(){
		$dokumen = Dokumen::get();
		return view('assignment',['dokumen' => $dokumen]);
	}
 
	public function proses_upload(Request $request){
		$this->validate($request, [
			'file' => 'required|file|mimes:pdf',
			'keterangan' => 'required',
		]);
 
		// menyimpan data file yang diupload ke variabel $file
		$file = $request->file('file');
 
		$nama_file = time()."_".$file->getClientOriginalName();
 
      	// isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'tugas';
		$file->move($tujuan_upload,$nama_file);
 
		Dokumen::create([
			'file' => $nama_file,
			'keterangan' => $request->keterangan,
		]);
 
		return redirect()->back();
	}

	public function hapus($id){
		// hapus file
		$dokumen = Dokumen::where('id',$id)->first();
		File::delete('tugas/'.$dokumen->file);
	 
		// hapus data
		Dokumen::where('id',$id)->delete();
	 
		return redirect()->back();
	}
}
