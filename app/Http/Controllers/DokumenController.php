<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Dokumen;
 
class DokumenController extends Controller
{
 
    // menampilkan data dokumen
    public function index()
    {
    	$dokumen = Dokumen::All();
    	return view('assignment', ['dokumen' => $dokumen]);
    }
}