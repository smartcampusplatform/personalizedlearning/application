<?php
 
 namespace App\Http\Controllers\Admin;
 
use Illuminate\Http\Request;
use App\Timeline;
 
use App\Course;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
// use App\Http\Controllers\Traits\FileUploadTrait;

class TimelineController extends Controller
{
 
    // menampilkan data timeline
    public function index()
    {
    	$timeline = Timeline::All();
    	return view('admin.timeline.index', ['timeline' => $timeline]);
    }
    public function good()
    {
    	$timeline = Timeline::All();
    	return view('/timeline', ['timeline' => $timeline]);
    }

}